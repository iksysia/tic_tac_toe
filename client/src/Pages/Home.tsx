import React, { useEffect, useState } from 'react';
import { routes } from 'constants/routes';
import { css } from "@emotion/react";
import { CustomLink } from "components/CustomLink";
import { CSSTransition } from 'react-transition-group';

export const styles = {
  container: () => css`
    margin: 0 auto;
    display: flex;
    align-items: center;
    justify-content: space-between;
    flex-direction: column;
    max-width: 1140px;
    width: 100%;
    min-height: 35vh;

    h1 {
      font-size: 42px;
      font-weight: 600;
    }
  `,
  buttonWrapper: () => css`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: space-between;
    min-height: 150px;
  `,
}

const Home = () => {
  const [inProp, setInProp] = useState(false);

  useEffect(() => {
    setInProp(true)
  }, []);

  return (
    <CSSTransition in={inProp} timeout={200} classNames="my-node">
      <div css={styles.container()}>
        <h1>Welcome into Tic-Tac-Toe Game</h1>
        <div css={styles.buttonWrapper}>
          <CustomLink title={'Start game'} link={routes.GAME} />
          <CustomLink title={'Play with Bot'} link={routes.GAMEWITHAI} />
        </div>
      </div>
    </CSSTransition>
  )
}
export default Home;
