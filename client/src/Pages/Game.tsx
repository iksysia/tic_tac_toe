import React, { useCallback, useEffect, useState } from 'react';
import { routes } from 'constants/routes';
import { styles } from './Home';
import { CustomLink } from 'components/CustomLink';
import Board from 'components/Board';
import { CustomButton } from 'components/CustomButton';
import { Winner } from 'components/Winner';
import { ErrorType, madeTurn, resetGame, startGame, StepType } from '../API/index';

const Game: React.FC = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [turn, setTurn] = useState<boolean>(true);
  const [board, setBoard] = useState<StepType[]|null[]>([]);
  const [winner, setWinner] = useState<StepType|null>(null);
  const [newError, setError] = useState<ErrorType>(null);

  const initGame = useCallback(async () => {
    setLoading(true);
    try {
      const data = await startGame();
      const {gameType, winner, storedGameField} = data;
      if (gameType === 'starts' || gameType === 'continues') {
        setBoard(storedGameField);
      }
      if (winner) {
        setWinner(winner)
      }
      setLoading(false)
    } catch (e) {
      setError(e);
      setLoading(false)
    }
  }, []);

  useEffect(() => {
    initGame();
  }, [initGame]);

  const handleStep = async (index: number) => {
    if (winner) return
    try {
      const {storedGameField, winner} = await madeTurn({type: turn, index})
      setTurn(!turn)
      setBoard(storedGameField)
      if (winner) {
        setWinner(winner);
        setBoard(storedGameField)
      }
    } catch (e) {
      console.log(e)
    }
  };

const handleResetGame = async () => {
    try {
      const {gameType, storedGameField, winner} = await resetGame();
      if (gameType === 'starts') {
        setWinner(winner)
        setBoard(storedGameField)
      }
    } catch (e) {
      setError(e);
      console.log(e)
    }
  }

  if (loading) {
    return (
      <div style={{textAlign: "center"}}>
        <h1 style={{fontSize: 42, marginBottom: 30}}>Loading</h1>
      </div>
    )
  }

  if (newError) {
    return (
      <div style={{textAlign: "center"}}>
        <h1 style={{fontSize: 42, marginBottom: 30}}>Bad Getaway :(</h1>
        <p style={{fontSize: 22, marginBottom: 30}}>server not responding, error code 500!</p>
        <CustomLink title={'Go back'} link={'/'}/>
      </div>
    )
  }

  return (
    <div css={styles.container()}>
      {winner && <Winner winner={winner.type}/>}
      {winner === null ?
        turn === true ?
          (
            <div>
              <h1>Cross turn</h1>
            </div>
          )
          : (
            <div>
              <h1>Zero turn</h1>
            </div>
          ) : null}
      <Board slots={board} onClick={handleStep}/>
      {winner && (<CustomButton onClick={handleResetGame} title={'Reset game'}/>)}
      <CustomLink link={routes.HOME} title={'Back'}/>
    </div>
  )
}

export default Game;
