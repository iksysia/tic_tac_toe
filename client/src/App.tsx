import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { css, Global } from '@emotion/react';
import { Routing } from './Routing';

const globalStyles = () => css`
  // Start css reset
  html, body, div, span, applet, object, iframe,
  h1, h2, h3, h4, h5, h6, p, blockquote, pre,
  a, abbr, acronym, address, big, cite, code,
  del, dfn, em, img, ins, kbd, q, s, samp,
  small, strike, strong, sub, sup, tt, var,
  b, u, i, center,
  dl, dt, dd, ol, ul, li,
  fieldset, form, label, legend,
  table, caption, tbody, tfoot, thead, tr, th, td,
  article, aside, canvas, details, embed,
  figure, figcaption, footer, header, hgroup,
  menu, nav, output, ruby, section, summary,
  time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    font: inherit;
    vertical-align: baseline;
  }

  /* HTML5 display-role reset for older browsers */

  article, aside, details, figcaption, figure,
  footer, header, hgroup, menu, nav, section {
    display: block;
  }

  body {
    line-height: 1;
  }

  ol, ul {
    list-style: none;
  }

  blockquote, q {
    quotes: none;
  }

  blockquote:before, blockquote:after,
  q:before, q:after {
    content: '';
  }

  table {
    border-collapse: collapse;
    border-spacing: 0;
  }

  // End css reset
  body {
    background: linear-gradient(#bbb, transparent 1px), linear-gradient(90deg, #bbb, transparent 1px), center, center;
    background-size: 15px 15px;
    font-family: 'Architects Daughter', cursive;
  }
  // Animations
  @keyframes draw {
    100% {
      stroke-dashoffset: 0;
    }
  }
`;

const styles = {
  wrapper: () => css`
    display: flex;
    align-items: center;
    justify-content: center;
    padding-top: 15vh;
    padding-bottom: 15vh;
    position: relative;
  `,
};

function App () {
  return (
    <Router>
      <div css={styles.wrapper}>
        <Global styles={globalStyles}/>
        <Routing/>
      </div>
    </Router>

  );
}

export default App;
