import axios, { AxiosResponse } from 'axios';

const urlApi = {
  dev: 'http://localhost:3000/api',
};

const routes = {
  start: '/start',
  turn: '/step',
  step: '/userStep',
  reset: '/reset',
}

const api = axios.create({
  baseURL: urlApi.dev,
});

export type GameType = 'continues' | 'finished' | 'starts';
export type StepType = {
  type: boolean;
  index: number;
}
export type StartGame = {
  winner: null|StepType;
  gameType: GameType;
  storedGameField: null[]|StepType[];
}
export type ErrorType = {
  error: string;
}

type MadeTurnResponse = {
  winner: null|StepType;
  gameType: GameType;
  storedGameField: StepType[];
}
type ResetGame = {
  winner: null|StepType;
  gameType: GameType;
  storedGameField: StepType[];
}

export const startGame = async () => {
  try {
    const response: AxiosResponse<StartGame> = await api.get(`${routes.start}`);
    return response.data;
  } catch (e) {
    throw new Error(e)
  }
}

export const madeTurn = async (data: StepType) => {
  try {
    const response: AxiosResponse<MadeTurnResponse> = await api.post(`${routes.turn}`, data);
    return response.data;
  } catch (e) {
    throw new Error(e)
  }
};

export const crossTurn = async (data: StepType) => {
  try {
    const response: AxiosResponse<MadeTurnResponse> = await api.post(`${routes.step}`, data);
    return response.data;
  } catch (e) {
    throw new Error(e)
  }
};

export const resetGame = async () => {
  try {
    const response: AxiosResponse<ResetGame> = await api.get(`${routes.reset}`);
    return response.data;
  } catch (e) {
    throw new Error(e)
  }
}
