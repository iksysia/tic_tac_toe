import React, { memo } from 'react';
import {Slot, SlotProps } from 'components/Slot';
import { css } from '@emotion/react';

type BoardProps = {
  slots: SlotProps[] | null[];
  onClick: (i:number) => void;
}

const gameStyles = {
  panel: () => css`
    width: 372px;
    height: 372px;
    display: flex;
    flex-wrap: wrap;
    margin: 30px 0;
  `,
  field: () => css`
    width: 120px;
    height: 120px;
    cursor: pointer;
    &:nth-child(2) {
      border-left: 2px solid #000;
      border-right: 2px solid #000;
    }
  `,
}

const Board: React.FC<BoardProps> = ({slots, onClick}) => {
  return (
    <div css={gameStyles.panel}>
      {
        slots.map((slot, index) =>
          (<Slot
            onClick={() => onClick(index)}
            key={index}
            id={slot?.index}
            type={slot?.type}
            index={index} />
          ))
      }
    </div>
  )
};

export default memo(Board);
