import React from 'react';
import { Link } from 'react-router-dom';
import { css } from '@emotion/react';

type CustomLinkProps = {
  title: string;
  link: string;
}

const styles = {
  link: () => css`
    padding: 10px 16px;
    background: #ff0000;
    color: #fff;
    text-decoration: none;
    outline: none;
    font-size: 24px;
    font-weight: 700;
    border-radius: 10px;
    transition: all 0.5s ease-in-out;
    box-shadow: 13px 12px 22px 0 rgba(0, 0, 0, 0.2);
    &:hover{
      transform: scale(1.1);
    }
  `,
}

export const CustomLink: React.FC<CustomLinkProps> = ({link, title}) => {
  return <Link css={styles.link()} to={link}>{title}</Link>
}
