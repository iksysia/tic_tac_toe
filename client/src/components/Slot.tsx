import React from 'react';
import { Cross } from 'components/icons/Cross';
import { Zero } from 'components/icons/Zero';
import { css } from '@emotion/react';

export type SlotProps = {
  active?: boolean;
  index?: number;
  id?: number;
  type?: boolean;
  onClick?: () => void
}

const styles = {
  wrapper: (active: boolean, index: number) => css`
    width: 122px;
    height: 123px;
    cursor: pointer;
    background: ${active ? 'green' : 'transparent'};
    border-top: ${index === 0 || index === 1 || index === 2 ? 'transparent' : '1px solid black'};
    border-left: ${index === 0 || index === 3 || index === 6 ? 'transparent' : '1px solid black'};
    border-right: ${index === 2 || index === 5 || index === 8 ? 'transparent' : '1px solid black'};
    border-bottom: ${index === 6 || index === 7 || index === 8 ? 'transparent' : '1px solid black'};
   
  `,
};

export const Slot: React.FC<SlotProps> = ({active, type , id= null, index, onClick}) => {
  return (
    <div css={styles.wrapper(active, index)}  onClick={onClick}>
      {id !== null
        ? (type === true ? <Cross /> : <Zero />)
        : null }
    </div>
  )
}
