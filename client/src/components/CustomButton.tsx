import React from 'react';
import { css } from '@emotion/react';

type CustomButtonProps = {
  title: string;
  onClick: () => void;
}

const styles = {
  button: () => css`
    padding: 10px 16px;
    background: #ff0000;
    color: #fff;
    text-decoration: none;
    outline: none;
    font-family: 'Architects Daughter', cursive;
    font-size: 24px;
    font-weight: 700;
    border-radius: 10px;
    transition: all 0.5s ease-in-out;
    box-shadow: 13px 12px 22px 0 rgba(0, 0, 0, 0.2);
    margin-top: 15px;
    margin-bottom: 15px;
    &:hover{
      transform: scale(1.1);
    }
  `,
}

export const CustomButton: React.FC<CustomButtonProps> = ({title, onClick}) => {
  return (
    <button css={styles.button()} type="reset" onClick={onClick}>{title}</button>
  )
}
