import React from "react";
import { css } from "@emotion/react";
import { animated, config, Transition } from "react-spring";

type WinnerProps = {
  winner: boolean;
}

const styles = {
  container: () => css`
    position: absolute;
    top: -25px;
  `,
}

export const Winner: React.FC<WinnerProps> = ({winner}) => {

  return (
    <div css={styles.container}>
      <Transition
        items={winner === false ? true : true}
        from={{opacity: 0}}
        enter={{opacity: 1}}
        leave={{opacity: 0}}
        reverse={winner === false ? true : true}
        delay={200}
        config={config.molasses}
      >
        {(styles, item) =>
          item && <animated.div style={styles}>
            <h1>{winner === true ? 'Cross' : 'Zero'} Won's!</h1>
          </animated.div>
        }
      </Transition>

    </div>

  )
};
