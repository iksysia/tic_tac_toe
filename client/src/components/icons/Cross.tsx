import React from 'react';
import { css } from "@emotion/react";

const cross = () => css`
  width: 100%;
  height: 100%;
  
  .first {
    stroke-dasharray: 125;
    stroke-dashoffset: 125;
    animation: draw 0.6s forwards;
  }
  .second {
    stroke-dasharray: 125;
    stroke-dashoffset: 125;
    animation: draw 0.6s 0.6s forwards;
`;
export const Cross: React.FC = () => {

  return (
    <div>
      <svg css={cross}>
        <line className="first" x1="15" y1="15" x2="100" y2="100" stroke="red" strokeWidth="10"
              strokeLinecap="round"/>
        <line className="second" x1="100" y1="15" x2="15" y2="100" stroke="red" strokeWidth="10"
              strokeLinecap="round"/>
      </svg>
    </div>
  )
};
