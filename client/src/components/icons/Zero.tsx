import React from 'react';
import { css } from "@emotion/react";

const zero = () => css`
  width: 100%;
  height: 100%;
  stroke-dasharray: 283;
  stroke-dashoffset: 283;
  animation: draw 1s forwards;
`;

export const Zero: React.FC = () => {
  return (
    <div>
      <svg css={zero}>
        <circle r="45" cx="58" cy="58" stroke="blue" strokeWidth="10" fill="none" strokeLinecap="round"/>
      </svg>
    </div>
  )
};
