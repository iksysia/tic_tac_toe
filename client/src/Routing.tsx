import React from 'react';
import { animated, useTransition } from 'react-spring'
import { Route, Routes, useLocation } from 'react-router-dom';
import { routes } from 'constants/routes';
import Home from './Pages/Home';
import Game from './Pages/Game';
import AiGame from './Pages/AiGame';
import { css } from "@emotion/react";

const styles = {
  page: () => css`
    position: absolute;
    width: 100%;
  `,

}
export const Routing: React.FC = () => {
  const location = useLocation();
  const transitions = useTransition(location, {
    keys: location.pathname,
    from: {
      opacity: 0,
    },
    enter: {
      opacity: 1,
    },
    leave: {
      opacity: 0,
    }
  });

  return (
    <div style={{position: 'relative', width: '100%' }}>
      {transitions((props, item) => (
        <animated.div style={props}>
          <div css={styles.page}>
            <Routes location={item}>
              <Route path={routes.HOME} element={<Home />}/>
              <Route path={routes.GAMEWITHAI} element={<AiGame />}/>
              <Route path={routes.GAME} element={<Game />}/>
            </Routes>
          </div>
        </animated.div>
      ))}

    </div>
  )
};
