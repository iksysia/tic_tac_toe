import { Router} from "express";
import GameController from "./Controller.js";

const router = new Router();

router.get('/start', GameController.start);
router.post('/step', GameController.step);
router.get('/reset', GameController.resetGame);

router.post('/userStep', GameController.userStep);

export default router;
