const lines = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];

export function calculateWinner(squares) {
  // console.log('squares', squares)
  for (let i = 0; i < lines.length; i++) {
    const [a, b, c] = lines[i]
    if (squares[a]?.type === squares[b]?.type && squares[a]?.type === squares[c]?.type) {
      return squares[a]
    }
  }
  return null
}

export function botStep(squares) {
  const crossSteps = squares.filter(square => square?.type === true)
  let zeroSteps = squares.map((square, index ) => {
    if (square === null) {
      return index
    }
  }).filter(square => square !== undefined)

  if (crossSteps.length === 1) {
    let randomZero = Math.floor(Math.random() * zeroSteps.length);
    if (crossSteps[0].index === 8 && randomZero === crossSteps[0].index) {
       randomZero = crossSteps[0].index - 1
    }
    if (crossSteps[0].index === 0 && randomZero === crossSteps[0].index) {
     randomZero = crossSteps[0].index + 1
    }
    if (randomZero === crossSteps[0].index) {
      randomZero = crossSteps[0].index + 1
    }

    zeroSteps = zeroSteps.filter(square => square !== randomZero);
    console.log('updated zeroSteps', zeroSteps)
    return {
      type: false,
      index: randomZero,
    };
  }

  for (let i = 0; i < lines.length; i++) {

    const [a, b, c] = lines[i]

    if (squares[a]?.type === true || squares[b]?.type === true || squares[c]?.type === true) {
      if (squares[a]?.type === squares[b]?.type && squares[c] === null){
        zeroSteps = zeroSteps.filter(square => square !== c);
        return {
          type: false,
          index: c,
        };
      }

      if (squares[a]?.type === squares[c]?.type && squares[b] === null){
        zeroSteps = zeroSteps.filter(square => square !== b);
        return {
          type: false,
          index: b,
        };
      }

      if (squares[b]?.type === squares[c]?.type && squares[a] === null){
        zeroSteps = zeroSteps.filter(square => square !== a);
        return {
          type: false,
          index: a,
        };
      }
    }
  }

  return {
    type: false,
    index: zeroSteps[Math.floor(Math.random() * zeroSteps.length)],
  }

}
