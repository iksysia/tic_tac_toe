import express from 'express';
import router from './Router.js';
import cors from 'cors';

const PORT = 3000;

const app = express();

app.use(express.json())
app.use(cors());
app.options('*', cors());
app.use('/api', router);

async function startServer() {
  try {
    app.listen(PORT, () => console.log('server starts!'))
  } catch (e) {
    console.log('TopLevel Error', e)
  }
}
startServer();
