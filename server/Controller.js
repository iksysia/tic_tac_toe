import GameService from "./Service.js";
class GameController {
   async start(req, res) {
    try {
      const field = await GameService.start();
      res.json(field)
    } catch (e) {
      console.log('start', e)
    }
  }

  async step(req, res) {
    try {
      const winner = await GameService.step(req.body);
      res.json(winner)
    } catch (e) {
      console.log('router error', e)
    }
  }

  async resetGame(req, res) {
    try {
      const reset = await GameService.resetGame();
      res.json(reset);
    } catch (e) {
      console.log('resetGame', e)
    }
  }

  async userStep(req, res) {
      try {
        const filed = await GameService.userStep(req.body);
        res.json(filed)
    } catch (e) {
      console.log('resetGame', e)
    }
  }
}

export default new GameController();
