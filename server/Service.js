import {botStep, calculateWinner} from './helpers/calculateWinner.js'
import store from 'store';


class GameService {

  async start() {
    const storedGameField = new Array(9).fill(null);
    try {
      await store.set('gameFiled', storedGameField);

      return {winner: null, gameType: 'starts', storedGameField}
    } catch (e) {
      console.log('start error', e)
    }
  }

  async step(turn) {
    const storedGameField = store.get('gameFiled');

    const gameField = storedGameField || new Array(9).fill(null);

    try {
      const {index, type} = turn;
      if (typeof index === 'number' && typeof type === "boolean") {
        gameField[index] = turn;
        await store.set('gameFiled', gameField);
        const winner = calculateWinner(gameField)

        if (winner !== null) {
          return {winner, gameType: 'finished', storedGameField};
        }
      }
      return {
        winner: null,
        gameType: 'continues',
        storedGameField: store.get('gameFiled')
      };

    } catch (e) {
      throw Error(e);
    }
  }

  async resetGame() {
    try {
      const storedGameField = await store.set('gameFiled', new Array(9).fill(null));
      console.log(storedGameField)
      return {winner: null, gameType: 'starts', storedGameField}
    } catch (e) {
      console.log('resetGame', e)
    }
  }

  async userStep(turn) {
    try {
      const storedGameField = store.get('gameFiled');
      const gameField = storedGameField || new Array(9).fill(null);
      let winner;

      gameField[turn.index] = turn;
      winner = calculateWinner(gameField);

      if (winner === null) {
        const zeroStep = botStep(gameField);
        console.log('zeroStep', zeroStep);

        if (zeroStep) {
          gameField[zeroStep.index] = zeroStep;
        }

        winner = calculateWinner(gameField);

        if (winner) {
          return {winner, gameType: 'finished', storedGameField: gameField};
        }
      }
      await store.set('gameFiled', gameField);
      return {winner, gameType: 'continues', storedGameField: gameField};
    } catch (e) {
      console.log('resetGame', e)
    }
  }

}

export default new

GameService();
