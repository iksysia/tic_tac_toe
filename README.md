# Tic_Tac_Toe



## Getting started

Clone this project.

First run server: **cd server && npm i && npm start**

Then in another terminal: **cd client && npm i && npm start**

## Open localhost:3001 link in your browser to enjoy playing Tic Tac Toe game.
